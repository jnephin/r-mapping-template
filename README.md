# R Mapping Template

## Summary
* Makes a series of raster layer maps.
* Can add point, line and polygon features on top of maps of raster layers.
* A number of colour palette's to choose from.

## Structure of working directory
* Data/Layers/ (directory with raster layers to map)
* Data/Overlays/ (directory with shapefile features for overlays)
* Coastline/ (location of land polygon to be used as base map)
* Maps/ (location to export maps)
